type Config = {
    readonly HOST: any,
    readonly PORT: any,
}

export const config: Config = {
    HOST: process.env.HOST || 'localhost',
    PORT: process.env.PORT || 8080
} 