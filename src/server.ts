import express, { Application } from 'express';
import http, { Server } from 'http';
import { config } from './config/config';
import { apiRouter } from './api/api-router';

export const app: Application = express();
export const server: Server = http.createServer(app);

// Setup express-middleware
app.use('/api', apiRouter);

// Start server
server.listen(config.PORT, () => {
    console.log('Server running on port: %s', config.PORT)
});