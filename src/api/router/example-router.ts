// TODO: This is an example how to use express router. Please delete if not needed anymore
import express, { Router, Request, Response } from 'express';

export const exampleRouter: Router = express.Router();

/** 
 * @swagger
 * /api/example:
 *  get:
 *    tags:
 *    - "ExampleRouter"
 *    description: This is an example router
 *    responses:
 *      '200':
 *        description: A successful response
 */
exampleRouter.get('/', (req: Request, res: Response) => {
    res.send('Hello example-router!');
});

/** 
 * @swagger
 * /api/example:
 *  post:
 *    tags:
 *    - "ExampleRouter"
 *    description: This is an example router
 *    responses:
 *      '200':
 *        description: A successful response
 */
exampleRouter.post('/', (req: Request, res: Response) => {
    res.send('successfull post');
});