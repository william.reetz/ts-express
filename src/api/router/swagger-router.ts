import swaggerJsDoc, { SwaggerDefinition } from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import express, { Router } from 'express';
export const swaggerRouter: Router = express.Router();

const swaggerDefinition: SwaggerDefinition = {
  info: {
    version: '1.0.1',
    title: 'ts-express',  
    description: 'campus card api'
  }
}

const swaggerOptions: any = {
  swaggerDefinition: swaggerDefinition,
  apis: ['**/*.ts']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);


swaggerRouter.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocs))