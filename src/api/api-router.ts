import express, { Router } from 'express';
import { swaggerRouter } from './router/swagger-router';
import { exampleRouter } from './router/example-router'; // Please delete if not needed anymore
export const apiRouter: Router = express.Router();

apiRouter.use('/docs', swaggerRouter);
apiRouter.use('/example', exampleRouter);