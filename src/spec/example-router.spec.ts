import  request  from "supertest";
import { server } from '../server';

describe('example-router', () => {
    it('should work', async () => {
        const result = await request(server).get("/api/example");
        expect(result.text).toEqual("Hello example-router!");
        expect(result.status).toEqual(200);
    })
});

afterAll(() => {
    server.close();
});