# TS Express
[![pipeline](https://gitlab.com/william.reetz/ts-express/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/william.reetz/ts-express/pipelines)
[![coverage](https://gitlab.com/william.reetz/ts-express/badges/master/coverage.svg?style=flat-square)](https://william.reetz.gitlab.io/ts-express/)
## Installation
```sh
# clone repository
git clone git@gitlab.com:william.reetz/ts-express.git
# change to ts-express directory
cd ts-express
# install dependencies
npm install
```
## Start
```sh
# transpile typescript
npm run build
# start server
npm start

# or

# start server with nodemon and auto-typescript-transpilation 
npm run dev
```

## Test
```sh
# run tests with jest
npm run test
```

## Debugging
### Debug- Setup for  Visual Studio Code
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Node Inspector",
            "type": "node",
            "request": "launch",
            "args": [
                "${workspaceRoot}/src/server.ts"
            ],
            "runtimeArgs": [
                "-r",
                "ts-node/register"
            ],
            "cwd": "${workspaceRoot}",
            "protocol": "inspector",
            "internalConsoleOptions": "openOnSessionStart",
            "env": {
                "TS_NODE_IGNORE": "false"
            }
        }
    ]
}
```